<?php
namespace App\Http\Controllers;

class PagesController extends Controller
{
public function home()
{
	$people = ['Én', 'a Gyula', 'meg az Ottó', 'a Tóth Ottó'];

    return view('pages.welcome', compact('people'));
}

public function about()
{

	return view('pages.about');
}
}
