@extends('layout')

@section('content')
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<h1> {{ $card->title }} </h1>

		<ul class="list-group">

			@foreach ($card->notes as $note)
				<form method="GET" action="/notes/{{ $note->id }}/edit">
				<li class="list-group-item row"><span class="col-md-10"> {{ $note->body }}</span> 
					<button type="submit" class="btn btn-warning col-md-1">
						<i class="glyphicon glyphicon-pencil" style="color: #fff;"></i>
					</button>
				</li>
				</form>
			@endforeach

		</ul>
<br><br>
<hr />
		<h3>Add a New Note</h3>

		<form method="POST" action="{{ $card->id }}/notes">

			<div class="form-group">
				<textarea name="body" class="form-control"></textarea>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Add Note</button>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			</div>
		</form>

	</div>
</div>
@stop